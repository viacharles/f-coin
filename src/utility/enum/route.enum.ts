export enum EModule {
  User = 'user',
  Business = 'business'
}

export enum EUserPage {
  Chat = 'chat'
}

export enum EIndividualPage {
  Landing = 'landing'
}

