
/**
 * @description 頁面配置
 */
export interface IPage {
  path: string;
  icon: string;
}
